//Завдання 1
const min = Math.floor(Math.random() * 60); // генеруємо рандомне число від 0 до 59
console.log(min);
if (min < 15) {
    console.log('Перша четверть');
} else if (min < 30) {
    console.log('Друга четверть');
} else if (min < 45) {
    console.log('Третя четверть');
} else {
    console.log('Четверта четверть');
}


// Завдання 2
let lang = "en";
let arr;

if (lang === "ua") {
    arr = ["Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота", "Неділя"];
} else if (lang === "en") {
    arr = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
} else {
    console.log("Помилка: невірне значення lang");
}

console.log(arr);

// Завдання 3
let str = "123454";
let sumFirstThree = parseInt(str[0]) + parseInt(str[1]) + parseInt(str[2]);
let sumLastThree = parseInt(str[3]) + parseInt(str[4]) + parseInt(str[5]);

if (sumFirstThree === sumLastThree) {
    console.log("Щасливий!");
} else {
    console.log("Вам не пощастило");
}
